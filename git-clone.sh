#!/bin/bash

if [ -n "$1" ]; then
    USER="$(sudo git config user.name)"
    sudo git clone "https://github.com/$USER/$1"
else
    echo "Usage: git-clone <local-repo>"
fi;
